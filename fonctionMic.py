#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pyaudio
import wave

def recordmic(timerec):
	FORMAT = pyaudio.paInt16	
	CHANNELS = 1				# Mono.
	RATE = 44100				# Frequence d'echantillonnage.
	BUFFER = 1024

	# Creation de l'objet.
	audio = pyaudio.PyAudio()
 
	# Enregistrement audio.
	stream = audio.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True, frames_per_buffer=BUFFER)
	frames = []
	for i in range(0, int(RATE / BUFFER * int(timerec))):
		data = stream.read(BUFFER)
		frames.append(data)
 
	# On arrete l'enregistrement.
	stream.stop_stream()
	stream.close()
	audio.terminate()

	# On enregistre notre fichier. 
	emplacement = "record.wav"
	waveFile = wave.open(emplacement, 'wb')
	waveFile.setnchannels(CHANNELS)
	waveFile.setsampwidth(audio.get_sample_size(FORMAT))
	waveFile.setframerate(RATE)
	waveFile.writeframes(b''.join(frames))
	waveFile.close()
	
	return emplacement