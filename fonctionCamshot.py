#!/usr/bin/python3
# -*- coding: utf-8 -*-

import cv2
import time

def camshot():
	camera_port = 0 # Camera 0 pour le port utilise par defaut par la webcam.
	camera = cv2.VideoCapture(camera_port) # On initialise un objet de capture qui renvoie vers le port par defaut.

	heure = time.strftime("%d:%B:%Y_%H:%M:%S_")
	emplacement = heure + "camshot.png"
    # Trouver un chemin absolu pour l'enregistrement, comme un repertoire de travaille.

	# Capturer une image.
	retval, camera_capture = camera.read() # Lire la camera.
	cv2.imwrite(emplacement, camera_capture)  # Sauvegarde.
#	del(camera)
	return emplacement