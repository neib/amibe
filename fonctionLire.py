#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle


def Lire(emplacement) :

	with open(emplacement, 'br') as fichier:
		mon_depickler = pickle.Unpickler(fichier)
		contenu = fichier.read()

	return contenu.decode("utf-8")