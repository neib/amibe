#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os


def shell(message):

		command = message.split() 
		if command[0] == "cd" :
			if len(command) > 1 :
				if os.path.isdir(command[1]) :
					os.chdir(command[1])
				else :
					return command[1]+ " n'est pas un repertoire valide."

		else :
			command = message
			handle = os.popen(command)
			line = handle.read()
			return line
			handle.close()