#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
from PyQt4.QtGui import QPixmap, QApplication
import time

def screenshot() :
	app = QApplication(sys.argv)
	heure = time.strftime("%d:%B:%Y_%H:%M:%S_")
	emplacement = heure + "screenshot.png"
    # Trouver un emplacement absolu dans un repertoire de travail.
	QPixmap.grabWindow(QApplication.desktop().winId()).save(emplacement, 'png')
	return emplacement