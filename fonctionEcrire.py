#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pickle


def Ecrire(emplacement, texte) :

	with open(emplacement, 'a') as fichier:
		fichier.write(str(texte).replace("', '", " "))